const express = require('express'),
      cors = require('cors'),
      bodyParser = require('body-parser'),
      cookieParser = require('cookie-parser'),
      productsRoutes = require('./src/routes/products'),
      authRoutes = require('./src/routes/auth'),
      sequelize = require('./src/sequelize')

require('dotenv').config()

const app = express()
const { PORT } = require('./src/config')

app.use(cors())
app.use(bodyParser.json())
app.use(cookieParser())
app.use('/api/products', productsRoutes)
app.use('/api/auth', authRoutes)

sequelize.sync().then(_ => app.listen(PORT))