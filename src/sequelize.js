const Sequelize = require('sequelize')

const sequelize = new Sequelize('products', 'newuser', 'newuser', {
  dialect: 'postgres',
  host: 'postgres',
  port: 5432,
  logging: false
})

module.exports = sequelize