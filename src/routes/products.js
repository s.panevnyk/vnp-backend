const { Router } = require('express')
const Product = require('../models/Product')

const router = Router()

router.get('/', async (_, res) => {
  try {
    const products = await Product.findAll()
    if (!products) throw new Error('Items not found')
    res.status(200).send(products)
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

router.post('/create', async (req, res) => {
  try {
    await Product.create(req.body)
    res.status(200).send()
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const response = await Product.findByPk(id)
    response.update(req.body)
    res.status(200).send()
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const response = await Product.findByPk(id)
    response.destroy()
    res.status(200).send()
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

module.exports = router
