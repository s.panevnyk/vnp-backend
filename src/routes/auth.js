const { Router } = require('express')

const router = Router()

router.post('/register', (req, res) => {
  try {
    res.status(200).send(req.body)
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

router.post('/login', (req, res) => {
  try {
    res.status(200).send(req.body)
  } catch ({ message }) {
    res.status(500).send({ message })
  }
})

module.exports = router
